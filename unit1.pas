unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Grids, Menus, ExtCtrls, ComCtrls, ShellCtrls, Process,LCLType,Unix,strutils;

type

  { TForm1 }

  TForm1 = class(TForm)
    btnExtract: TButton;
    btnLoadMlv: TButton;
    btnRemove: TButton;
    btnRender: TButton;
    btnMoreVidOptions: TButton;
    btnReloadDiscInfo: TButton;
    btnInfo: TButton;
    cbFps: TComboBox;
    cbFrameType: TComboBox;
    cbVideoType: TComboBox;
    GroupBox1: TGroupBox;
    gbFrameSettings: TGroupBox;
    gbVideoSettings: TGroupBox;
    MessageBox: TMemo;
    OpenDialogMlv: TOpenDialog;
    OpenDialogXmp: TOpenDialog;
    rbToFrames: TRadioButton;
    rbToVideo: TRadioButton;
    Shape: TShape;
    StringGrid1: TStringGrid;
    Timer1: TTimer;
    Timer2: TTimer;
    procedure btnInfoClick(Sender: TObject);
    procedure btnReloadDiscInfoClick(Sender: TObject);
    procedure btnExtractClick(Sender: TObject);
    procedure btnLoadMlvClick(Sender: TObject);
    procedure btnRemoveClick(Sender: TObject);
    procedure btnRenderClick(Sender: TObject);
    procedure cbFpsChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure gbVideoSettingsClick(Sender: TObject);
    procedure rbToVideoChange(Sender: TObject);
    procedure ShapePaint(Sender: TObject);
    procedure StringGrid1DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure StringGrid1DrawCell(Sender: TObject; aCol, aRow: Integer;
      aRect: TRect; aState: TGridDrawState);
    procedure StringGrid1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure StringGrid1Selection(Sender: TObject; aCol, aRow: Integer);
    procedure Timer1Timer(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;




type
  TVideo = class(TObject)
    path,Xmp,name: String;
    extracted: boolean;
    extract:boolean;
    render:boolean;
    rendered:boolean;
    frames: integer;
    onWorkExtracting:boolean;
    onWorkRendering:boolean;
  public
    constructor Create(inputPath :String);
    constructor Create(inputPath :String; inputExtracted: Boolean; inputFrames: integer; inputXmp: String);
    function getPath(): String;
    function getName(): String;
    function getFrames(): integer;
    function getXmp(): String;
    function isExtracted(): boolean;
    function isToExtract(): boolean;
    function isExtracting(): boolean;

    function isToRender(): boolean;
    function isRendered():boolean;
    function isRendering():boolean;

    procedure setFrames(input :integer);
    procedure setXmp(input: String);
    procedure setExtracted(input :boolean);
    procedure setToExtract(input :boolean);
    procedure setExtracting(input :boolean);

    procedure setToRender(input :boolean);
    procedure setRendered(input :boolean);
    procedure setRendering(input :boolean);


  end;

type
  TDisc = class(TObject)
    mountingPoint:String;
    mpSubfolder:integer;
    size:integer;
    freeSpace:integer;
    extractSize:integer;
    inUse:boolean;
  public
    constructor Add(inputMountingPoint :String;inputSize: integer;inputFreeSpace: integer);
    function getPath():String;
    function getSize():integer;
    function getFreeSpace():integer;
    function getSubfolders():integer;
    function getExtractSize(): integer;
    procedure setExtractSize(input :integer);
    function isInUse(): boolean;
    procedure setInUse(input: boolean);
  end;





var
  Form1: TForm1;
  discList: array of TDisc;
  videoList: array of TVideo;
  selectedVideos: array of boolean;
  workingExtract:boolean=false;
  workingExtractIndex:integer;
  workingRender:boolean=false;
  workingRenderIndex:integer;
  ExtractProcesses: array of Tprocess;
  RenderProcesses: array of Tprocess;
  MessageSkipLine: Integer;
  renderTime: Integer;
  RenderEta8:integer;
  RenderEta7:integer;
  RenderEta6:integer;
  RenderEta5:integer;
  RenderEta4:integer;
  RenderEta3:integer;
  RenderEta2:integer;
  RenderEta1:integer;
  ExtractSize:double;
  LastOpenFolder:String;
  dfOutput:TStringList;
  RunDv:TProcess;

implementation

{$R *.lfm}

{ TForm1 }



constructor TDisc.Add(inputMountingPoint: String;inputSize: integer;inputFreeSpace: integer);
  var
    Delimiter:TSysCharSet;
  begin
    mountingPoint:=inputMountingPoint;
    size:=inputSize;
    freeSpace:=inputFreeSpace;
    Delimiter:=['/'];
    mpSubfolder:=WordCount(mountingPoint,Delimiter);

  end;

function TDisc.getPath(): String;
  begin
    Result:=mountingPoint;
  end;

function TDisc.getSize(): integer;
  begin
    Result:=size;
  end;

function TDisc.getFreeSpace(): integer;
  begin
    Result:=freeSpace;
  end;

function TDisc.getSubfolders(): integer;
  begin
    Result:=mpSubfolder;
  end;

function TDisc.getExtractSize(): integer;
  begin
    Result:=extractSize;
  end;

procedure TDisc.setExtractSize(input: integer);
  begin
    ExtractSize:=input;
  end;


function TDisc.isInUse(): boolean;
  begin
    Result:=inUse;
  end;

procedure TDisc.setInUse(input: boolean);
  begin
    inUse:=input;
  end;



constructor TVideo.Create(inputPath :String);
  begin
    path:=inputPath;
    extracted:=false;
    extract:=false;
    name:=Copy(ExtractFileName(inputPath),0,length(ExtractFileName(inputPath))-4);
    onWorkExtracting:=false;
    onWorkRendering:=false;
    Xmp:='';
    render:=false;
    rendered:=false;


  end;

constructor TVideo.Create(inputPath :String; inputExtracted: Boolean; inputFrames: integer; inputXmp: String);
  begin
    path:=inputPath;
  end;

function TVideo.getPath(): String;
  begin
    Result := path;
  end;

function TVideo.getName(): String;
  begin
    Result := name;
  end;

function TVideo.getFrames(): integer;
  begin
    Result := frames;
  end;

function TVideo.getXmp(): String;
  begin
    Result := Xmp;
  end;

procedure TVideo.setFrames(input: integer);
  begin
    frames := input;
  end;

procedure TVideo.setXmp(input: String);
  begin
    Xmp := input;
  end;

procedure TVideo.setExtracted(input: boolean);
  begin
    extracted:=input;
  end;

function TVideo.isExtracted():boolean;
  begin
    Result := extracted;
  end;


procedure TVideo.setToExtract(input: boolean);
  begin
    extract := input;
  end;

function TVideo.isToExtract():boolean;
  begin
    Result := extract;
  end;


function TVideo.isExtracting():boolean;
  begin
    Result := onWorkExtracting;
  end;

procedure TVideo.setExtracting(input: boolean);
  begin
    onWorkExtracting := input;
  end;






function TVideo.isToRender():boolean;
  begin
    Result := render;
  end;

function TVideo.isRendered():boolean;
  begin
    Result := rendered;
  end;

function TVideo.isRendering():boolean;
  begin
    Result := onWorkRendering;
  end;

procedure TVideo.setToRender(input: boolean);
  begin
    render := input;
  end;

procedure TVideo.setRendered(input: boolean);
  begin
    rendered := input;
  end;

procedure TVideo.setRendering(input: boolean);
  begin
    onWorkRendering := input;
  end;









type
  TVidArray = array of TVideo;
procedure DeleteVideo(var AArray: TVidArray; const AIndex: Integer);
begin
  Move(AArray[AIndex + 1], AArray[AIndex], SizeOf(AArray[0]) * (Length(AArray) - AIndex - 1)); //Dahinterliegende Daten aufrücken
  SetLength(AArray, Length(AArray) - 1); // Länge kürzen
end;

type
  TSelArray = array of boolean;
procedure DeleteSelected(var AArray: TSelArray; const AIndex: Integer);
begin
  Move(AArray[AIndex + 1], AArray[AIndex], SizeOf(AArray[0]) * (Length(AArray) - AIndex - 1)); //Dahinterliegende Daten aufrücken
  SetLength(AArray, Length(AArray) - 1); // Länge kürzen
end;




procedure UpdateDiscs;
  var
    disc:TDisc;
    i,c:integer;
    dfParts: TStringList;
    Delimiters:TSysCharSet;
  begin
      RunDv.Execute;
      dfOutput.LoadFromStream(RunDv.Output);
      setLength(discList,0);

      dfParts:=TStringList.Create;
      for i:=1 to dfOutput.count-1 do
        begin
          Delimiters := [' '];
          for c:=1 to 6 do
            begin
              dfParts.add(ExtractWord(c,dfOutput[i],Delimiters));
            end;

          disc:=TDisc.Add(dfParts[5],strtoint(dfParts[1]),strtoint(dfParts[3]));

          setLength(discList,length(disclist)+1);
          discList[length(discList)-1]:=disc;

          dfParts.clear;


        end;

     { for c:=0 to length(discList)-1 do begin
      Form1.MessageBox.Lines.add(discList[c].getPath+' '+inttostr(discList[c].getSize)+' '+inttostr(discList[c].getFreeSpace)+' '+inttostr(discList[c].getSubfolders));
      end;   }





  end;




procedure PaintDiscInfo;

var
  X2,X1,i,j,d,c,maxSubfolders,selectedDisc,distance:integer;
  checkedDiscs: array of integer;
  BarLength:double;
  AProcess:TProcess;
  Disc,cmd,mp,videoPath:string;
  scipdisc:boolean;
begin

  for i:=0 to length(DiscList)-1 do         //clean
    begin
       DiscList[i].setInUse(false);
       DiscList[i].setExtractSize(0);
    end;





  scipdisc:=false;
  ExtractSize:=0;
  for i:=0 to length(videolist)-1 do
    begin

        while length(checkedDiscs)<length(DiscList) do
             begin
               maxSubfolders:=0;
               for d:=0 to length(discList)-1 do
                 begin

                    for c:=0 to length(checkedDiscs)-1 do
                      begin
                         if d=checkedDiscs[c] then scipdisc:=true;
                      end;

                    if scipdisc=false then
                      begin
                        if discList[d].getSubfolders >= maxSubFolders then
                          begin
                            maxSubfolders:=discList[d].getSubfolders;
                            selectedDisc:=d;
                          end;

                      end;
                    scipdisc:=false;
                 end;
               setLength(checkedDiscs,length(checkedDiscs)+1);
               checkedDiscs[length(checkedDiscs)-1]:=selectedDisc;
               mp:=DiscList[selectedDisc].getPath;        //mounting point
               videoPath:=VideoList[i].getPath;
               if mp=copy(videoPath,1,length(mp)) then
                 begin
                   //Form1.Label1.Caption:= mp+'   '+copy(videoPath,1,length(mp));
                   DiscList[selectedDisc].setInUse(true);
                   if videoList[i].isToExtract() then
                     begin
                       ExtractSize:=Filesize(ExtractFilePath(videoList[i].getPath)+videoList[i].getName()+'.mlv');
                       DiscList[selectedDisc].setExtractSize( DiscList[selectedDisc].getExtractSize+round(ExtractSize));
                     end;
                   break;
                 end;
            end;
        setLength(checkedDiscs,0);
    end;

  {Form1.Label4.caption:='';
  Form1.Label1.caption:='0';
  for i:=0 to length(DiscList)-1 do
    begin
       Form1.Label1.caption:=inttostr( strtoint(Form1.Label1.caption)+DiscList[i].getExtractSize);
       if DiscList[i].isInUse then  Form1.Label4.caption:=Form1.Label4.caption +'true '
       else
        Form1.Label4.caption:=Form1.Label4.caption +'false ';
    end;
           }




  distance:=50;
  c:=0;

  for i:=0 to length(DiscList)-1 do
    begin
      if DiscList[i].inUse then c:=c+1;
    end;
  c:=c-1;
  if c>=0 then
    begin
      Form1.height:= 453+(c*distance);
      Form1.MessageBox.top:=80+(c*distance);
      Form1.StringGrid1.top:=80+(c*distance);
      Form1.GroupBox1.top:=80+(c*distance);
      Form1.cbFps.Top:=84+(c*distance);
      Form1.btnLoadMlv.top:=384+(c*distance);
      Form1.btnRemove.top:=384+(c*distance);
      Form1.btnExtract.top:=384+(c*distance);
      Form1.btnRender.top:=384+(c*distance);
      Form1.Shape.height:=65+(c*distance);
      Form1.btnReloadDiscInfo.top:=56+(c*distance);
      Form1.btnInfo.top:=416+(c*distance);


     { Form1.Label1.top:=416+(c*distance);
      Form1.Label4.top:=433+(c*distance);      }

    end
  else
   begin
      Form1.height:= 453;
      Form1.MessageBox.top:=80;
      Form1.StringGrid1.top:=80;
      Form1.GroupBox1.top:=80;
      Form1.cbFps.Top:=84;
      Form1.btnLoadMlv.top:=384;
      Form1.btnRemove.top:=384;
      Form1.btnExtract.top:=384;
      Form1.btnRender.top:=384;
      Form1.Shape.height:=65;
      Form1.btnReloadDiscInfo.top:=56;
      Form1.btnInfo.top:=416;
      Form1.Shape.canvas.brush.Color:=clwhite;
      Form1.Shape.canvas.font.Color:=clblack;
      Form1.Shape.canvas.textOut(Form1.Shape.Left+9,Form1.Shape.Top+7,'Disc info... ');

    end;



  c:=0;
  for j:=0 to length(DiscList)-1 do
    begin
      if DiscList[j].inUse then
        begin

          Form1.Shape.canvas.Brush.Color:=$0096D8E8;
          //Form1.Shape.canvas.rectangle(Form1.Shape.Left+25,Form1.Shape.Top+(15+c*distance),Form1.Shape.Width+Form1.Shape.left-25,Form1.Shape.Top+30+(c*distance));
          Form1.Shape.canvas.rectangle(Form1.Shape.Left+9,Form1.Shape.Top+(7+c*distance),Form1.Shape.Width+Form1.Shape.left-41,Form1.Shape.Top+23+(c*distance));
          Barlength:=(Form1.Shape.Width-50);
          X2:=round(((DiscList[j].getSize-DiscList[j].getFreeSpace)/DiscList[j].getSize)*Barlength);
          Form1.Shape.canvas.Brush.Color:=$00836E5A;
          Form1.Shape.canvas.rectangle(Form1.Shape.left+9,Form1.Shape.Top+(7+c*distance),X2+Form1.Shape.left+9,Form1.Shape.Top+23+(c*distance));
          Form1.Shape.canvas.brush.Color:=clwhite;
          Form1.Shape.canvas.font.Color:=clblack;
          Form1.Shape.canvas.textOut(Form1.Shape.Left+9,Form1.Shape.Top+(27+c*distance),'Size of Disc: '+Inttostr(round(DiscList[j].getSize/(1024*1024)))+'GiB  |  Free Space:'+Inttostr(round(DiscList[j].getFreeSpace/(1024*1024)))+'GiB  |  '+DiscList[j].getPath);

          //Form1.Label1.caption:=inttostr(round(((DiscList[j].getSize-DiscList[j].getFreeSpace)/DiscList[j].getSize)*100));


          if DiscList[j].getExtractSize > 0 then
            begin
              Form1.shape.canvas.Brush.Color:=clRed;
              X1:=X2+Form1.shape.left+8;
              X2:=X2+Form1.shape.left+9+round(((DiscList[j].getExtractSize/1024)/DiscList[j].getSize)*Barlength);
              Form1.shape.canvas.rectangle(X1,Form1.shape.Top+7+(c*distance),X2,Form1.shape.Top+23+(c*distance));
            end;



          c:=c+1;
        end;
    end;




  {if ExtractSize>0 then
    begin
      canvas.Brush.Color:=clRed;
      X1:=X2+shape.left+24;
      X2:=X2+shape.left+25+round((ExtractSize/DiskSize(0))*Barlength);
      canvas.rectangle(X1,shape.Top+15,X2,shape.Top+30);
    end;  }

    //Form1.Shape.Refresh;

end;






procedure FillStringGrid;                      /////Fill Grid  (& create open and archivate buttons)
  var
    c:integer;
    lengthSec:integer;
    lengthMin:integer;
  begin
    if length(videoList)>0 then
      begin
        Form1.StringGrid1.RowCount:=length(videoList)+1;
        for c:=0 to length(videoList)-1 do
          begin
            Form1.StringGrid1.cells[0,c+1]:=ExtractFileName(videoList[c].getPath);

            if videoList[c].isToExtract()=true then
              begin
                Form1.StringGrid1.cells[1,c+1]:='Yes';
              end
            else
              begin
                Form1.StringGrid1.cells[1,c+1]:='No';
              end;


            if videoList[c].isExtracted() then
              begin
                Form1.StringGrid1.cells[1,c+1]:='Done';
                Form1.StringGrid1.cells[2,c+1]:=Inttostr(videoList[c].getframes);

                lengthSec:=round(videoList[c].getframes()/StrToInt(Form1.cbFps.Text));
                lengthMin:=0;
                while lengthSec>59 do
                  begin
                     lengthSec:=lengthSec-60;
                     lengthMin:=lengthMin+1;
                  end;
                Form1.StringGrid1.cells[3,c+1]:=InttoStr(lengthMin)+'m, '+InttoStr(lengthSec)+'s';
              end;

            if videoList[c].getXmp()<>'' then
               begin
                 Form1.StringGrid1.Cells[4,c+1]:=ExtractFileName(videoList[c].getXmp());
               end;


            if videoList[c].isToRender()=true then
              begin
                Form1.StringGrid1.cells[5,c+1]:='Yes';
              end
            else
              begin
                if videoList[c].getXmp<>'' then
                  Form1.StringGrid1.cells[5,c+1]:='No';
              end;




          end;
      end
    else
      begin
        Form1.StringGrid1.RowCount:=1;
      end;
  end;





procedure SaveSession;           ///.session
var
  i:integer;
  settings:Tmemo;
begin
  settings:=TMemo.Create(Form1);
  settings.append('###rawJobs session file###');
  for i:=0 to length(videoList)-1 do
    begin
      settings.append('---video'+inttostr(i)+'---');
      settings.append('DIR_______:'+videoList[i].getPath());
      if videoList[i].isExtracted() then
         begin
         settings.append('EXTRACTED_:1');
         settings.Append('FRAMES____:'+inttostr(+videoList[i].getFrames()));
         if videoList[i].getXmp()<>'' then
           begin
             settings.Append('XMP_______:'+videoList[i].getXmp);
             if videoList[i].isRendered() then settings.Append('RENDERED__:1')
               else settings.Append('RENDERED__:0');
           end;
         end
      else
         begin
           settings.append('EXTRACTED_:0');
         end;

    end;
  settings.Lines.append('---end---');
  settings.Lines.SaveToFile(getcurrentdir()+'/.session');

end;


procedure SaveVideoInfo;         ////*.rjf
var
  i:integer;
  settings:Tmemo;
begin
  for i:=0 to length(videoList)-1 do
    begin
      settings.Clear;
      settings.append('###rawJobs video session file###');
      settings.append('---video---');
      if videoList[i].isExtracted() then
         begin
         settings.append('EXTRACTED_:1');
         settings.Append('FRAMES____:'+inttostr(+videoList[i].getFrames()));
         if videoList[i].getXmp()<>'' then
           begin
             settings.Append('XMP_______:'+videoList[i].getXmp);
             if videoList[i].isRendered() then settings.Append('RENDERED__:1')
               else settings.Append('RENDERED__:0');
           end;
         end
      else
         begin
           settings.append('EXTRACTED_:0');
         end;
      settings.Lines.SaveToFile(ExtractFilePath(videoList[i].getPath())+videoList[i].getname()+'.rjf');
    end;
end;



procedure OpenSession;
var
  i,j,c,index,oldIndex:integer;
  settings:Tmemo;
  video:TVideo;
  stopLoading:boolean;
begin
  settings:=TMemo.Create(Form1);
  if fileExists(getcurrentdir+'/.session') then settings.lines.LoadFromFile(getcurrentdir+'/.session');
  i:=0;
  c:=0;
  index:=-1;
  oldIndex:=-1;
  stopLoading:=false;
  while i<settings.Lines.Count do
     begin
       if settings.Lines[i]='---video'+inttostr(c)+'---' then
         begin
            j:=i+1;
            while (settings.Lines[j]<>'---video'+inttostr(c+1)+'---') AND (settings.Lines[j]<>'---end---') do
               begin


                 if copy(settings.lines[j],1,11)='DIR_______:' then
                   begin

                     if DirectoryExists(ExtractFilePath(copy(settings.lines[j],12,length(settings.lines[j])))) then
                       begin

                         video:=TVideo.Create(copy(settings.lines[j],12,length(settings.lines[j])));
                         SetLength(videoList,length(videolist)+1);
                         SetLength(selectedVideos,length(selectedVideos)+1);

                         videoList[length(videoList)-1] := video;
                         selectedVideos[length(selectedVideos)-1] :=false;
                         index:=c;
                         c:=c+1;

                       end
                     else
                       begin
                         showMessage('Directory '+ExtractFilePath(copy(settings.lines[j],12,length(settings.lines[j])))+' does not exist.'+ sLineBreak + sLineBreak +
                           'Skipping file '+Copy(ExtractFileName(copy(settings.lines[j],12,length(settings.lines[j]))),0,length(ExtractFileName(copy(settings.lines[j],12,length(settings.lines[j]))))) );
                         c:=c+1;
                         break;
                       end;
                   end;

                 if (index=-1) OR (index=oldIndex) then begin
                   showMessage('Corrupted session file. Cannot load session...');
                   stopLoading:=true;
                   break;
                 end;


                 if settings.lines[j]='EXTRACTED_:1' then videoList[length(videoList)-1].setExtracted(true);
                 if settings.lines[j]='EXTRACTED_:0' then videoList[length(videoList)-1].setExtracted(false);
                 if copy(settings.lines[j],1,11)='FRAMES____:' then
                 begin
                   videoList[length(videoList)-1].setFrames(strtoint(copy(settings.lines[j],12,length(settings.lines[j]))));
                   if FindAllFiles(ExtractFilePath(videolist[length(videoList)-1].getPath()),'*.dng',true).count<>strtoint(copy(settings.lines[j],12,length(settings.lines[j]))) then
                      showMessage('Frame count differs from extracted frames for file '+videoList[length(videoList)-1].getname+'.'+ sLineBreak + sLineBreak +'I found '+inttostr(FindAllFiles(ExtractFilePath(videolist[length(videoList)-1].getPath()),'*.dng',true).count)+' frames'
                      +' but there should be '+copy(settings.lines[j],12,length(settings.lines[j]))+' frames.');
                 end;

                 if settings.lines[j]='RENDERED__:1' then videoList[length(videoList)-1].setRendered(true);
                 if settings.lines[j]='RENDERED__:0' then videoList[length(videoList)-1].setRendered(false);
                 if copy(settings.lines[j],1,11)='XMP_______:' then
                   begin
                     if fileExists(copy(settings.lines[j],12,length(settings.lines[j]))) then
                       videoList[length(videoList)-1].setXmp(copy(settings.lines[j],12,length(settings.lines[j])))
                     else
                       showMessage('selected *.xmp for video '+videoList[length(videoList)-1].getname+' is missing... '+ sLineBreak + sLineBreak + '('+copy(settings.lines[j],12,length(settings.lines[j]))+')');
                   end;




                 j:=j+1;
                 if j>settings.lines.count-1 then break;

               end;
            oldIndex:=index;


         end;
       if stopLoading=true then break;
       i:=i+1;
     end;
  FillStringGrid;
  PaintDiscInfo;
end;







                                 ////////////// EXTRACTION \\\\\\\\\\\\\\\









procedure TForm1.btnExtractClick(Sender: TObject);
var
  i:integer;
  cmd,dir:string;
  AProcess:Tprocess;

begin

  btnRemove.enabled:=false;
  btnLoadMlv.enabled:=false;
  btnExtract.enabled:=false;
  btnRender.enabled:=false;

  ///////////////////////Create Array of Processes

  SetLength(ExtractProcesses,0);
  SetLength(ExtractProcesses,length(videolist));


  for i:=0 to length(videolist)-1 do
    begin
      if videolist[i].isToExtract()=true then
         begin
           dir:=getcurrentdir();

           cmd:='xterm -e "echo Extracting '+ExtractFileName(videolist[i].getPath())+';'
                       +'cd '+ExtractFilePath(videolist[i].getPath())+';'
                       //+'mkdir '+videolist[i].getName+';'
                       //+'mv '+ExtractFileName(videolist[i].getPath())+' '+videolist[i].getName+';'
                       +'cp '+dir+'/mlv2dng .;'
                       //+'cd '+videolist[i].getName+';'
                       +'chmod +x mlv2dng;'
                       +'./mlv2dng '+ExtractFileName(videolist[i].getPath())+';'
                       +'echo Done"';//+';'+'bash"';
           AProcess:=TProcess.Create(nil);
           AProcess.CommandLine:=cmd;
           //AProcess.Options := AProcess.Options + [poWaitOnExit];
           ExtractProcesses[i]:=AProcess;
         end;
    end;


  ////////////////////////Enables Execution of Processes at Timer1

  MessageSkipLine:=0;
  Form1.MessageBox.Clear;
  workingExtractIndex:=0;
  workingExtract:=true;    //not used at poWaitOnExit option
  btnExtract.Caption:='...working';
  PaintDiscInfo;

end;


procedure TForm1.Timer1Timer(Sender: TObject);
var
  i:integer;
  frameList:TStringList;
  anim1,anim2,anim3,anim4,anim0:string;
begin

   //////////////////////Execute Processes & Fill MessageBox


   if workingExtract=true then
      begin
         if workingExtractIndex<=length(ExtractProcesses)-1 then
           begin
              if ExtractProcesses[workingExtractIndex]<>nil then
                begin
                  if Form1.MessageBox.Lines[workingExtractIndex-MessageSkipLine]='' then
                    Form1.MessageBox.Lines[workingExtractIndex-MessageSkipLine]:='Extracting '+videolist[workingExtractIndex].getName;

                  if ExtractProcesses[workingExtractIndex].Running=false then
                     begin
                       if videolist[workingExtractIndex].isExtracting()=false then
                         begin                                                       ///Start
                           ExtractProcesses[workingExtractIndex].Execute;
                           videolist[workingExtractIndex].setExtracting(true);
                         end
                       else
                         begin                                                       ///End
                            videolist[workingExtractIndex].setExtracting(false);
                            videolist[workingExtractIndex].setToExtract(false);
                            ExtractProcesses[workingExtractIndex].Free;
                            if FileExists(ExtractFilePath(videolist[workingExtractIndex].getPath())+'000000.dng') then
                               begin
                                 Form1.MessageBox.Lines[workingExtractIndex-MessageSkipLine]:='Extracting '+videolist[workingExtractIndex].getName+'...' +' Done';
                                 videolist[workingExtractIndex].setExtracted(true);
                                 frameList:=FindAllFiles(ExtractFilePath(videolist[workingExtractIndex].getPath()),'*.dng',true);
                                 videolist[workingExtractIndex].setFrames(frameList.count);
                                 UpdateDiscs;
                                 PaintDiscInfo;
                               end
                            else
                               begin
                                 Form1.MessageBox.Lines[workingExtractIndex-MessageSkipLine]:='Extracting '+videolist[workingExtractIndex].getName+'...' +' failed';
                               end;
                            workingExtractIndex:=workingExtractIndex+1;


                            SaveSession;
                            SaveVideoInfo;   //*.rjf
                            Shape.Paint;
                            FillStringGrid;
                         end;

                     end
                  else          ///while running
                    begin
                       anim0:= 'Extracting '+videolist[workingExtractIndex].getName;
                       anim1:= 'Extracting '+videolist[workingExtractIndex].getName+'|..';
                       anim2:= 'Extracting '+videolist[workingExtractIndex].getName+'.|.';
                       anim3:= 'Extracting '+videolist[workingExtractIndex].getName+'..|';
                       anim4:= 'Extracting '+videolist[workingExtractIndex].getName+'...';

                       if Form1.MessageBox.Lines[workingExtractIndex-MessageSkipLine]=anim0 then
                         Form1.MessageBox.Lines[workingExtractIndex-MessageSkipLine]:=anim1;

                       if Form1.MessageBox.Lines[workingExtractIndex-MessageSkipLine]=anim4 then
                          Form1.MessageBox.Lines[workingExtractIndex-MessageSkipLine]:=anim1 else
                            if Form1.MessageBox.Lines[workingExtractIndex-MessageSkipLine]=anim1 then
                               Form1.MessageBox.Lines[workingExtractIndex-MessageSkipLine]:=anim2 else
                                 if Form1.MessageBox.Lines[workingExtractIndex-MessageSkipLine]=anim2 then
                                    Form1.MessageBox.Lines[workingExtractIndex-MessageSkipLine]:=anim3 else
                                    Form1.MessageBox.Lines[workingExtractIndex-MessageSkipLine]:=anim4;


                    end;
                 end
              else
                begin
                  workingExtractIndex:=workingExtractIndex+1;
                  MessageSkipLine:=MessageSkipLine+1;
                end;
           end
         else
           begin
             workingExtract:=false;
             btnExtract.Caption:='Extract';
             btnLoadMlv.enabled:=true;
             btnRender.enabled:=true;
           end;
      end;
end;








                                   ////////////// RENDERING \\\\\\\\\\\\\\\






procedure TForm1.btnRenderClick(Sender: TObject);
var
  i:integer;
  cmd,frameSettings,videoSettings:string;
  AProcess:Tprocess;

begin

  btnRemove.enabled:=false;
  btnLoadMlv.enabled:=false;
  btnExtract.enabled:=false;
  btnRender.enabled:=false;

  ///////////////////////Create Array of Processes

  SetLength(RenderProcesses,0);
  SetLength(RenderProcesses,length(videolist));

  ///settings

  if Form1.cbFrameType.Text='tiff' then
     begin
       frameSettings:='';       ///iwerpreifung upassen op render done oder fail. get op tiff getest
     end;


  if Form1.rbToFrames.checked=true then
     videoSettings:=''
  else
     begin
     end;



  for i:=0 to length(videolist)-1 do
    begin
      if videolist[i].isToRender()=true then
         begin
           Form1.MessageBox.Clear;
           Form1.MessageBox.Lines[0]:='#Generating Script';
           Form1.MessageBox.Lines[1]:='';
           Form1.MessageBox.Append('Xmp='+ExtractFileName(videoList[i].getXmp()));
           Form1.MessageBox.Append('fname2=0');
           Form1.MessageBox.Append('for file in ./*dng ; do');
           Form1.MessageBox.Append('     fname=$(basename "$file")');
           Form1.MessageBox.Append('     echo "Rendering: $fname"');
           Form1.MessageBox.Append('     mv $Xmp $fname.xmp');
           Form1.MessageBox.Append('     Xmp=$fname.xmp');
           Form1.MessageBox.Append('     darktable-cli $fname $Xmp $fname2.tiff');
           Form1.MessageBox.Append('     fname2=`expr $fname2 + 1`');
           Form1.MessageBox.Append('done');
           Form1.MessageBox.Append('');
           Form1.MessageBox.Append('exit');
           Form1.MessageBox.Append('#End');

           Form1.MessageBox.Lines.SaveToFile(ExtractFilePath(videoList[i].getPath())+'rendering.sh');
           Form1.MessageBox.Clear;

           cmd:='xterm -e "echo Start Rendering...;'
                       +'cd '+ExtractFilePath(videoList[i].getPath())+';'
                       +'chmod +x rendering.sh;'
                       +'sh rendering.sh;"';
                       //+'bash"';

           AProcess:=TProcess.Create(nil);
           AProcess.CommandLine:=cmd;
           RenderProcesses[i]:=AProcess;
         end;
    end;

  ////////////////////////Enables Execution of Processes at Timer1

  RenderTime:=0;
  MessageSkipLine:=0;
  workingRenderIndex:=0;
  workingRender:=true;    //not used at poWaitOnExit option
  btnRender.Caption:='...working';

end;




procedure TForm1.Timer2Timer(Sender: TObject);
var
  eta_min,eta_sec,eta_hou,i,currFrame:integer;
  eta_sec_old1,eta_sec_old2,eta_sec_old3,eta_sec_new:integer;
  anim1,anim2,anim3,anim4,anim0,prog,eta,fps:string;

begin

   //////////////////////Execute Processes & Fill MessageBox


   if workingRender=true then
      begin
         if workingRenderIndex<=length(RenderProcesses)-1 then
           begin
              if RenderProcesses[workingRenderIndex]<>nil then
                begin
                  if Form1.MessageBox.Lines[workingRenderIndex-MessageSkipLine]='' then
                    Form1.MessageBox.Lines[workingRenderIndex-MessageSkipLine]:='Rendering '+videolist[workingRenderIndex].getName;

                  if RenderProcesses[workingRenderIndex].Running=false then
                     begin
                       if videolist[workingRenderIndex].isRendering()=false then
                         begin                                                       ///Start
                           RenderProcesses[workingRenderIndex].Execute;
                           videolist[workingRenderIndex].setRendering(true);
                           RenderTime:=0;
                         end
                       else
                         begin                                                       ///End
                            videolist[workingRenderIndex].setRendering(false);
                            videolist[workingRenderIndex].setToRender(false);
                            RenderProcesses[workingRenderIndex].Free;
                            if FileExists(ExtractFilePath(videolist[workingRenderIndex].getPath())+'0.tif') then
                               begin
                                 Form1.MessageBox.Lines[workingRenderIndex-MessageSkipLine]:='Rendering '+videolist[workingRenderIndex].getName+'...' +' Done';
                                 videolist[workingRenderIndex].setRendered(true);
                                 UpdateDiscs;
                                 PaintDiscInfo;

                               end
                            else
                               begin
                                 Form1.MessageBox.Lines[workingRenderIndex-MessageSkipLine]:='Rendering '+videolist[workingRenderIndex].getName+'...' +' failed';
                               end;
                            workingRenderIndex:=workingRenderIndex+1;

                            SaveSession;
                            SaveVideoInfo;   //*.rjf
                            FillStringGrid;
                         end;

                     end
                  else          ///while running
                    begin

                       RenderTime:=RenderTime+1;

                       currframe:=FindAllFiles(ExtractFilePath(videolist[workingRenderIndex].getPath()),'*.tif',true).count;
                       prog:='['+inttostr(currframe)+'/'+inttostr(videoList[workingRenderIndex].getFrames())+']';
                       fps:= 'fps: '+Floattostr(round((currFrame/Rendertime)*100)/100);



                       if RenderTime>7 then RenderEta8:=RenderEta7;
                       if RenderTime>6 then RenderEta7:=RenderEta6;
                       if RenderTime>5 then RenderEta6:=RenderEta5;
                       if RenderTime>4 then RenderEta5:=RenderEta4;
                       if RenderTime>3 then RenderEta4:=RenderEta3;
                       if RenderTime>2 then RenderEta3:=RenderEta2;
                       if RenderTime>1 then RenderEta2:=RenderEta1;
                       if RenderTime>0 then RenderEta1:=round((videoList[workingRenderIndex].getFrames()-currFrame)*(Rendertime/currframe));

                       if RenderTime>7 then
                         begin
                           eta_sec:=round((RenderEta1+RenderEta2+RenderEta3+RenderEta4+RenderEta5+RenderEta6+RenderEta7+RenderEta8)/8)-4;
                           if eta_sec<0 then eta_sec:=0;

                           if currframe>0 then
                              begin
                                //eta_sec:=round((videoList[workingRenderIndex].getFrames()-currFrame)*(Rendertime/currframe));
                                eta_min:=0;
                                eta_hou:=0;
                                while eta_sec>59 do
                                  begin
                                    eta_sec:=eta_sec-60;
                                    eta_min:=eta_min+1;
                                  end;
                                while eta_min>59 do
                                  begin
                                    eta_min:=eta_min-60;
                                    eta_hou:=eta_hou+1;
                                  end;

                                if eta_hou>0 then
                                  eta:= 'ETA: '+inttostr(eta_hou)+'h '+ inttostr(eta_min)+'m '+inttostr(eta_sec)+'s'
                                else
                                  if eta_min>0 then
                                    eta:= 'ETA: '+ inttostr(eta_min)+'m '+inttostr(eta_sec)+'s'
                                  else
                                    eta:= 'ETA: '+inttostr(eta_sec)+'s';

                              end;

                         end
                       else
                              eta:= 'ETA: ?';
                       Form1.MessageBox.Lines[workingRenderIndex-MessageSkipLine]:='Rendering '+videolist[workingRenderIndex].getName+'... '+prog+'  '+fps+'  '+eta;



                    end;
                 end
              else
                begin
                  workingRenderIndex:=workingRenderIndex+1;
                  MessageSkipLine:=MessageSkipLine+1;
                end;
           end
         else
           begin
             workingRender:=false;
             btnRender.Caption:='Render';
             btnLoadMlv.enabled:=true;
             btnRender.enabled:=true;
           end;
      end;

end;














procedure TForm1.btnLoadMlvClick(Sender: TObject);
var
  video:TVideo;
  c,i:integer;
  fileAlreadyLoaded:boolean=false;
  fname,path:String;
begin

  if LastOpenFolder<>'' then OpenDialogMlv.InitialDir:=LastOpenFolder;
  if OpenDialogMlv.Execute then
  begin

     for i:=0 to OpenDialogMlv.Files.Count-1 do
       begin
         fname:=Copy(ExtractFileName(OpenDialogMlv.Files.Strings[i]),0,length(ExtractFileName(OpenDialogMlv.Files.Strings[i]))-4);
         path:=ExtractFilePath(OpenDialogMlv.Files.Strings[i])+fname+'/'+fname+'.mlv';
         video:=TVideo.Create(path);
         video.setToExtract(false);

         if length(videoList)>0 then
           begin
             for c:=0 to length(videoList)-1 do
                begin

                  if video.getPath() =  videoList[c].getPath() then
                     fileAlreadyLoaded:=true;

                end;
           end;

         if fileAlreadyLoaded=true then
           begin
              ShowMessage (video.getName()+' already loaded');
           end
         else
           begin
             if DirectoryExists(ExtractFilePath(video.getPath())) then
               showMessage ('Directory '+ExtractFilePath(video.getPath())+' already exists.'+ sLineBreak + sLineBreak +
                           'Please remove or rename this directory in order to load this file.'+sLineBreak +sLineBreak +'Skipping file '+video.getName()+'.mlv...' )
             else
               begin

                 CreateDir(ExtractFilePath(video.getPath()));
                 RenameFile(OpenDialogMlv.Files.Strings[i],ExtractFilePath(video.getPath())+video.getName()+'.mlv');


                 SetLength(videoList,length(videolist)+1);
                 SetLength(selectedVideos,length(selectedVideos)+1);

                 videoList[length(videoList)-1] := video;
                 selectedVideos[length(selectedVideos)-1] :=false;
                 LastOpenFolder:=ExtractFilePath(OpenDialogMlv.Filename);




                end;
           end;
       end;
     FillStringGrid;
     //SaveVideoInfo;
     SaveSession;
     PaintDiscInfo;
     Form1.Shape.Refresh;
  end
  else
      // Öffnen wurde abgebrochen, OpenDialog1.FileName ist leer
  ;

end;

procedure TForm1.btnRemoveClick(Sender: TObject);
var
  c:integer;
  render:boolean=false;
  removable:boolean=false;
  extractable:boolean=false;
begin
  c:=0;
  while c <= length(videolist)-1 do
     begin
        if selectedVideos[c]=true then
          begin
             DeleteSelected(selectedVideos,c);
             DeleteVideo(videoList,c);
          end
        else
          begin
             c:=c+1;
          end;
     end;

  btnRemove.enabled:=false;
  FillStringGrid;                                       //
                                                        //
  if length(selectedVideos)>0 then                      // in some cases the computer selects a cell after resizing the stringgrid
    begin                                               // this is to avoid an automatically selected video
      for c:=0 to length(selectedVideos)-1 do           //
         begin                                          //
            selectedVideos[c]:=false;                   //
         end;                                           //
    end;                                                //
  FillStringGrid;                                       //



  for c:=0 to length(videoList)-1 do
    begin
      if videoList[c].isToExtract=true then extractable:=true;
      if videoList[c].isToRender=true then render:=true;
      if selectedVideos[c]=true then removable:=true;
    end;

  if extractable = true then btnExtract.enabled:=true else btnExtract.enabled:=false;
  if render = true then btnRender.enabled:=true else btnRender.enabled:=false;
  if removable = true then btnRemove.enabled:=true else btnRemove.enabled:=false;
  SaveSession;
  PaintDiscInfo;
  Shape.Refresh;





end;







procedure TForm1.cbFpsChange(Sender: TObject);
begin
  //if cbFps.text='' then cbFps.text:='1';
  FillStringGrid;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
   dfOutput:= TStringList.Create;
   RunDv:=TProcess.Create(nil);
   RunDv.CommandLine:='df';
   RunDv.Options:=RunDv.Options + [poWaitOnExit, poUsePipes];
   UpdateDiscs;

   Form1.StringGrid1.ColWidths[0]:=150;
   Form1.StringGrid1.ColWidths[1]:=70;
   Form1.StringGrid1.ColWidths[2]:=70;
   Form1.StringGrid1.ColWidths[3]:=100;
   Form1.StringGrid1.ColWidths[4]:=140;
   Form1.StringGrid1.ColWidths[5]:=70;
   OpenSession;
end;





procedure TForm1.gbVideoSettingsClick(Sender: TObject);
begin

end;

procedure TForm1.rbToVideoChange(Sender: TObject);
begin
  if Form1.rbToVideo.Checked=true then
    begin
      Form1.gbVideoSettings.enabled:=true;
      btnMoreVidOptions.enabled:=true;
    end
  else
    begin
      Form1.gbVideoSettings.enabled:=false;
      btnMoreVidOptions.enabled:=false;
    end;
end;




procedure TForm1.ShapePaint(Sender: TObject);
begin
   PaintDiscInfo;
end;




procedure TForm1.btnReloadDiscInfoClick(Sender: TObject);
begin
   UpdateDiscs;
   PaintDiscInfo;
   Shape.Refresh;
end;

procedure TForm1.btnInfoClick(Sender: TObject);
begin
 showMessage('rawJobs v0.2'+ sLineBreak +  sLineBreak + 'License GPLv3');
end;




procedure TForm1.StringGrid1DragDrop(Sender, Source: TObject; X, Y: Integer);
begin

end;



                                       ////Coloring Grid

procedure TForm1.StringGrid1DrawCell(Sender: TObject; aCol, aRow: Integer;
  aRect: TRect; aState: TGridDrawState);
begin


   if aCol=1 then
     begin
       if aRow>0 then
         begin
           if StringGrid1.Cells[1,aRow]='Yes' then
              begin
                StringGrid1.canvas.brush.color:=clRed;
                StringGrid1.canvas.FillRect(aRect);
                StringGrid1.Canvas.TextRect(aRect, aRect.Left+2, aRect.Top, StringGrid1.Cells[ACol, ARow]);
              end
           else
              begin
                StringGrid1.canvas.brush.color:=clWhite;
                StringGrid1.canvas.FillRect(aRect);
                StringGrid1.Canvas.TextRect(aRect, aRect.Left+2, aRect.Top, StringGrid1.Cells[ACol, ARow]);
              end;
         end;

     end;

   if aCol=5 then
     begin
       if aRow>0 then
         begin
           if StringGrid1.Cells[5,aRow]='Yes' then
              begin
                StringGrid1.canvas.brush.color:=clLime;
                StringGrid1.canvas.FillRect(aRect);
                StringGrid1.Canvas.TextRect(aRect, aRect.Left+2, aRect.Top, StringGrid1.Cells[ACol, ARow]);
              end
           else
              begin
                StringGrid1.canvas.brush.color:=clWhite;
                StringGrid1.canvas.FillRect(aRect);
                StringGrid1.Canvas.TextRect(aRect, aRect.Left+2, aRect.Top, StringGrid1.Cells[ACol, ARow]);
              end;
         end;

     end;

   if aCol=0 then
     begin
       if aRow>0 then
         begin
           if length(selectedVideos)>0 then
             begin
                  if selectedVideos[aRow-1]=true then
                    begin
                       StringGrid1.canvas.brush.color:=clBlue;
                       StringGrid1.canvas.FillRect(aRect);
                       StringGrid1.Canvas.TextRect(aRect, aRect.Left+2, aRect.Top, StringGrid1.Cells[ACol, ARow]);
                     end
                   else
                     begin
                       StringGrid1.canvas.brush.color:=clWhite;
                       StringGrid1.canvas.FillRect(aRect);
                       StringGrid1.Canvas.TextRect(aRect, aRect.Left+2, aRect.Top, StringGrid1.Cells[ACol, ARow]);
                     end;

             end;
         end;

     end;


end;

                              //////select xmp file

procedure TForm1.StringGrid1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  BoxStyle,sgHeight,row:integer;
  Reply:Tmemo;

begin
  if workingExtract=false then
     begin
       if workingRender=false then
         begin
           sgHeight:=Form1.StringGrid1.DefaultRowHeight*Form1.StringGrid1.RowCount;

           if X>390 then
              begin
                if X<530 then
                   begin
                      if Y>Form1.StringGrid1.DefaultRowHeight then
                         begin
                           if Y<sgHeight then
                             begin
                                row:=(Y)div Form1.StringGrid1.DefaultRowHeight;
                                if videoList[row-1].isExtracted() then
                                   begin
                                      OpenDialogXmp.InitialDir:=ExtractFilePath(videoList[row-1].getPath());
                                      //Label4.caption:=ExtractFilePath(videoList[row-1].getPath());
                                      if OpenDialogXmp.Execute then
                                        begin

                                           if ExtractFilePath(OpenDialogXmp.FileName)=ExtractFilePath(videoList[row-1].getPath()) then
                                             videoList[row-1].setXmp(OpenDialogXmp.Filename)
                                           else
                                             if application.MessageBox('The selected xmp is in a different folder than the video files. I will need to copy it to the correct folder.', 'warning!', mb_YESNO Or MB_ICONQUESTION) = ID_YES then
                                                begin
                                                  copyFile(OpenDialogXmp.Filename,ExtractFilePath(videoList[row-1].getPath())+ExtractFileName(OpenDialogXmp.Filename));
                                                  videoList[row-1].setXmp(ExtractFilePath(videoList[row-1].getPath())+ExtractFileName(OpenDialogXmp.Filename));
                                                end

                                        end;
                                   end;
                             end;
                         end;

                   end;

              end;
            FillStringGrid;
            SaveSession;
            SaveVideoInfo;
        end;

     end;
end;







procedure TForm1.StringGrid1Selection(Sender: TObject; aCol, aRow: Integer);


var
  c:integer;
  extractable:boolean=false;
  removable:boolean=false;
  render:boolean=false;

begin                                  /////select files to extract

  if workingExtract=false then
  begin
    if workingRender=false then
      begin

        if aCol=1 then
          begin
            if length(videoList)>0 then
                begin
                  if videoList[aRow-1].isToExtract=true then
                    videoList[aRow-1].setToExtract(false)
                  else
                    if videoList[aRow-1].isToExtract=false then
                      begin
                        if videoList[aRow-1].isExtracted=false then videoList[aRow-1].setToExtract(true);
                      end;

                  for c:=0 to length(videoList)-1 do
                    if videoList[c].isToExtract=true then
                      extractable:=true;

                  if extractable = true then btnExtract.enabled:=true else btnExtract.enabled:=false;
                end;
            end;


        if aCol=5 then               ////select files to render
          begin
            if length(videoList)>0 then
                begin
                  if videoList[aRow-1].isToRender=true then
                    videoList[aRow-1].setToRender(false)
                  else
                    if videoList[aRow-1].isToRender=false then
                      begin
                        if videoList[aRow-1].isRendered=false then
                           if videoList[aRow-1].getXmp<>'' then videoList[aRow-1].setToRender(true);
                      end;

                  for c:=0 to length(videoList)-1 do
                    if videoList[c].isToRender=true then
                      render:=true;

                  if render = true then btnRender.enabled:=true else btnRender.enabled:=false;
                end;
            end;



        if aCol=0 then                      /////select files to remove
          begin
            if aRow>0 then
              begin
                if length(selectedVideos)>0 then
                  begin
                    if selectedVideos[aRow-1]=false then
                    selectedVideos[aRow-1]:=true
                  else
                    selectedVideos[aRow-1]:=false
                  end;

                for c:=0 to length(selectedVideos)-1 do
                  begin
                    if selectedVideos[c]=true then
                      begin
                        removable:=true;
                      end
                  end;
              end;
            if removable=true then btnRemove.enabled:=true else btnRemove.enabled:=false;

          end;

        FillStringGrid;
        PaintDiscInfo;
        Shape.Refresh;


    end;
  end
 else
  begin


  end;

end;



end.

